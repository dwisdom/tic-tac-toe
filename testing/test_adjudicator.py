# Terrible hack, there must be a better way to do this
import sys
sys.path.append('./src/')

from adjudicator import Adjudicator
from player import Player

def test_init():
    p1 = Player('x')
    p2 = Player('o')
    adj = Adjudicator(p1, p2)

    assert adj is not None
