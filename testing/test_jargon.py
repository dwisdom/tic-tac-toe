# Terrible hack, there must be a better way to do this
import sys
sys.path.append('./src/')

import jargon

def test_softmax_list_identity():
    start = [0.25] * 4
    result = jargon.softmax_list(start)
    assert start == result

def test_softmax_dict_identify():
    start = {1:0.25, 2:0.25, 3:0.25, 4:0.25}
    result = jargon.softmax_dict(start)
    assert start == result
