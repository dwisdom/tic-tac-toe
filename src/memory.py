from jargon import softmax_dict

class Memory:
    """
    A class that holds a dictionary of all the legal moves in each state
    that the AI has encountered

    All of the indices are based on this board layout:

    0 | 1 | 2
    3 | 4 | 5
    6 | 7 |8


    State is represented as a 9-character string of "X", "O", and " ". This
    state is the key to a dictionary.

    Each dictionary entry holds another dictionary where the keys are legal
    move indices and the values are the probability of selecting that move
    """

    def __init__(self, learn_rate: float = 1.0):
        self._learn_rate = learn_rate
        self._seen_states = {}

    def get_learn_rate(self):
        return self._learn_rate

    def set_learn_rate(self, new_value: float):
        self._learn_rate = new_value

    def _insert(self, state_key: str):
        """
        Inserts a state into the memory. This method also creates the dictionary
        of legal moves if the state isn't already in the memory

        :param state_key: A 9-character string of "X", "O", and " " representing
                            the game state
        """
        if state_key not in self._seen_states.keys():
            move_probs = {}
            num_possibilities = state_key.count(" ")
            # Get the legal moves to put in the dictionary
            for position in range(len(state_key)):
                if state_key[position] == " ":
                    # Initialize probability with a uniform distribution
                    move_probs[position] = 1 / num_possibilities
            # Insert the state into the dictionary
            self._seen_states[state_key] = move_probs

    def get_move_probs(self, state_key) -> dict:
        """
        Returns the dictionary of move probabilities for the game state

        :param state_key: A 9-character string of "X", "O", and " " representing
                          the game state
        :return: A dictionary of the form
                 {legal move index: probability to select that move}
        """
        if state_key not in self._seen_states.keys():
            self._insert(state_key)
        probs = self._seen_states[state_key]
        return probs

    def update_probs(self, moves_dict: dict, outcome: str):
        """
        After an AI plays a game, it calls this function with the moves it made
        during that game and final outcome of the game so that we can update
        the probabilities associated with the game states the AI saw.

        Win:
        Increment the winning move by the learning rate
        Softmax the probabilites

        Loss:
        Decrement the losing move by the learning rate
        Softmax the probabilities

        Draw: Do nothing


        :param moves_dict: A dictionary of {state_key: selected_move} for one
                            game that the AI played
        :param outcome: A string describing the final outcome of the game.
                        "w": Win
                        "l": Loss
                        "d": Draw
        """
        if outcome not in ['w', 'l', 'd']:
            raise ValueError("Memory().update_probs() called with game outcome " + str(outcome))

        if outcome == 'd':
            return

        # Win
        if outcome == 'w':

            # This should probably be moved to another function

            # Go through all the states that the AI saw
            for state in moves_dict.keys():
                move_played = moves_dict[state]
                probs = self.get_move_probs(state)

                # Increment the winning move
                probs[move_played] += self.get_learn_rate()

                # Softmax the probabilities
                probs = softmax_dict(probs)

                # Set the Memory's policy to match the updated policy
                self._seen_states[state] = probs

        # Loss
        else:
            # Go through all the states that the AI saw
            for state in moves_dict.keys():
                move_played = moves_dict[state]
                probs = self.get_move_probs(state)

                # Decrement the losing move
                probs[move_played] -= self.get_learn_rate()

                # Softmax the probabilities
                probs = softmax_dict(probs)

                # Set the Memory's policy to match the updated policy
                self._seen_states[state] = probs


if __name__ == "__main__":
    # mem = Memory()
    # my_state = "  XO OX  "
    # print(mem.get_move_probs(my_state))

    # I really need to write some unit tests

    # Create a fake game and see if it updates the probabilities
    state_0 = "         "
    state_1 = "X        "
    state_2 = "X  O     "
    state_3 = "XX O     "
    state_4 = "XX OO    "
    state_5 = "XXXOO    "
    move_0 = 0
    move_1 = 3
    move_2 = 1
    move_3 = 4
    move_4 = 2

    x_game = {state_0: move_0, state_2: move_2, state_4: move_4}
    o_game = {state_1: move_1, state_3: move_3}

    x_mem = Memory(learn_rate=1)
    o_mem = Memory(learn_rate=1)

    x_mem.update_probs(x_game, 'w')
    o_mem.update_probs(o_game, 'l')

    x_probs = x_mem.get_move_probs(state_0)
    print(x_probs)
    sum_x = 0
    for key in x_probs.keys():
        sum_x += x_probs[key]
    print(sum_x)
    print()

    o_probs = o_mem.get_move_probs(state_1)
    print(o_probs)
    sum_o = 0
    for key in o_probs.keys():
        sum_o += o_probs[key]
    print(sum_o)
