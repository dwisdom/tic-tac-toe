from random import choices

from memory import Memory

class Player:

    def __init__(self, side):
        self._side = side
        self._memory = Memory()
        self._moves_played_this_game = {}

    def get_side(self):
        return self._side

    def set_side(self, new_value):
        self._side = new_value

    def play_best_move(self, board):
        """
        Given a Board, the Player plays the best move on that board
        and then stores that board state and move in the single-game memory

        :param board: A Board where the Player will play a move
        """
        move = self._query_memory(board)
        self._remember_move(board.get_game_state(), move)
        board.play_move(self.get_side(), move)

    def _query_memory(self, board):
        """
        Ask the memory which move to make in a given board state

        :param board: A Board where the Player needs to make a move
        :return: A number [0,8] indicating where to play on the Board
        """
        board_state = board.get_game_state()
        probs_dict = self._memory.get_move_probs(board_state)
        moves = probs_dict.keys()
        probs = [probs_dict[move] for move in moves]
        # Cast the moves to int for random.choices()
        moves = [int(x) for x in moves]
        # Pick a move from the legal moves based on the policy
        move = choices(moves, weights=probs, k=1)
        # Returns a list of one item, so get the first one out of it
        return move[0]

    def _remember_move(self, board_state, move):
        """
        Insert a move into the running dicitonary of moves we've played in a game

        The dictionary looks like
        {board state (a string of 9 characters): move played (a number from 0-8)}

        :param board_state: A string of 9 characters representing the board
        :param move: Where the Player played on that board
        """
        # cast to str b/c lists aren't hashable
        self._moves_played_this_game[board_state] = move

    def react_to_game_over(self, outcome):
        """
        Tells the Memory that the game ended in a win/loss/draw so that the Memory can
        update the policy for the moves that the Player saw

        Also clears the single-game memory

        :param outcome: Whether the game ended as a win/loss/draw for this player,
                        denoted as "w", "l", or "d" respectively.
        """

        self._memory.update_probs(self._moves_played_this_game, outcome)

        self._moves_played_this_game = {}
      