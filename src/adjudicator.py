from board import Board
from player import Player

class Adjudicator:

    def __init__(self, player_1, player_2):

        if player_1.get_side().upper() == 'X':
            self._player_x = player_1
            self._player_o = player_2
        elif player_1.get_side().upper() == 'O':
            self._player_x = player_2
            self._player_o = player_1

        self._board = Board()

    def play_turn(self, player):
        """
        Gets a player to play a move on the board
        """
        player.play_best_move(self._board)

    def play_game(self):
        """
        Plays a game between two Players and returns the winner ('X', 'O', or 'draw')

        """
        # Can't win on the first two moves
        self.play_turn(self._player_x)
        self.play_turn(self._player_o)
        self.play_turn(self._player_x)
        self.play_turn(self._player_o)

        # Play moves until the game ends
        while True:
            self.play_turn(self._player_x)
            game_over = self._board.game_over()
            if game_over[0]:
                self._send_result_to_players(game_over[1])
                print(self._board)
                return game_over[1]

            self.play_turn(self._player_o)
            game_over = self._board.game_over()
            if game_over[0]:
                self._send_result_to_players(game_over[1])
                print(self._board)
                return game_over[1]

    def _send_result_to_players(self, winner):
        """
        Get each player to update their Memory based on the outcome of the game

        :param winner: Who won the game ('X', 'O', or 'draw')
        """
        if winner == 'X':
            self._player_x.react_to_game_over('w')
            self._player_o.react_to_game_over('l')
            return
        elif winner == 'O':
            self._player_x.react_to_game_over('l')
            self._player_o.react_to_game_over('w')
            return
        else:
            self._player_x.react_to_game_over('d')
            self._player_o.react_to_game_over('d')
            return


if __name__ == "__main__":

    first_player = Player('X')
    second_player = Player('O')
    adj = Adjudicator(first_player, second_player)
    result = adj.play_game()
    print(result)
    print('New initial policy for ', first_player.get_side())
    print(first_player._memory.get_move_probs('         '))
    print('New initial policy for ', second_player.get_side())
    print(second_player._memory.get_move_probs('X        '))
    print(second_player._memory.get_move_probs(' X       '))
    print(second_player._memory.get_move_probs('  X      '))
    print(second_player._memory.get_move_probs('   X     '))
    print(second_player._memory.get_move_probs('    X    '))
    print(second_player._memory.get_move_probs('     X   '))
    print(second_player._memory.get_move_probs('      X  '))
    print(second_player._memory.get_move_probs('       X '))
    print(second_player._memory.get_move_probs('        X'))
