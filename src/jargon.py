import math

def softmax_list(probs: list)->list:
    """
    Takes a list of numbers and returns the softmax of that list.

    In other words, it rescales everything to be in the range [0,1]
    and the sum of all the elements is 1

    :param probs:  A list of numbers to be softmax'd
    :return:       The softmax'd list
    """
    div = 0
    # There might be a way to do this without a loop, but the maximum number of
    # iterations is 9
    for i in probs:
        div += math.exp(i)

    return [math.exp(x)/div for x in probs]

def softmax_dict(probs: dict)->dict:
    """
    Takes a dictionary and softmaxes all the dictionary entries

    :param probs:  A dictionary of values to be softmaxed
    :return:       A dictionary with the same keys but with the values softmax'd
    """
    div = 0
    for val in probs.values():
        div += math.exp(val)

    for k in probs.keys():
        probs[k] = math.exp(probs[k])/div

    return probs
