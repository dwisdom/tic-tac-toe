import numpy as np


class Board:

    def __init__(self):
        # Represent the game as a string of characters
        self.game_state = ' ' * 9
        # self.game_state = np.array([x for x in self.game_state])
        self.num_moves = 0
        self.whos_turn = 'X'  # Could probably tell whose turn it is just by odd/even # of moves

        # Keep track of pieces played in each row, column, and diagonal
        self.cell_counts = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
        # Keep track of the row, column and diagonal sums
        self.row0_sum = 0
        self.row1_sum = 0
        self.row2_sum = 0
        self.col0_sum = 0
        self.col1_sum = 0
        self.col2_sum = 0
        self.dia0_sum = 0
        self.dia1_sum = 0

        self.list_of_sums = [self.row0_sum, self.row1_sum, self.row2_sum,
                             self.col0_sum, self.col1_sum, self.col2_sum,
                             self.dia0_sum, self.dia1_sum]

    def __repr__(self):
        """
        Print the board state.

        A board state is encoded as a string of 9 characters. Each charcter is
        a space, an "X", or an "O". Case shouldn't matter.

        This function reads the encoded board and puts it on a
        tic-tac-toe board from the top left to the bottom right

         0 | 1 | 2
         3 | 4 | 5
         6 | 7 | 8

        """

        board_state = self.game_state.upper()

        # Barebones input validation
        in_len = len(board_state)
        if in_len != 9:
            print('Invalid board state! Must be 9 tokens (got ' + str(in_len) + ')')
            return

        for token in board_state:
            if not self.valid_token(token):
                print('Invalid board state! Tokens must be " ", "X", or "O"' +
                      '(got "' + str(token) + '")')

            is_ = token == ' '
            is_x = token == 'X'
            is_x = token == 'O'
            is_valid = is_ or is_x or is_x
            if not is_valid:
                print('Invalid board state! Tokens must be " ", "X", or "O" (got "' + str(token) + '")')
                return

        # Print the board

        # Give it some spacing
        print()

        hline = '-----------\n'

        # Build the board
        board_to_print = ''

        # 3 rows
        for row_index in range(3):

            # Build a row
            row_to_add = ''
            for col_index in range(3):
                token_index = (3 * row_index) + col_index
                row_to_add += ' ' + board_state[token_index] + ' |'

            # Replace the last '|' with '\n'
            row_to_add = row_to_add[:-1] + '\n'
            # Put the row on the board
            board_to_print += row_to_add
            # Put in a horizontal line unless it's the last row
            if row_index != 2:
                board_to_print += hline

        return board_to_print

    def __eq__(self, other) -> bool:
        """
        Checks to see whether two Boards are the same

        :param other: The board to compare it to
        :return: bool
        """
        if self.game_state == other.game_state:
            return True
        return False

    def get_game_state(self):
        """
        :return: The board state, which is a list of nine characters
        """
        return self.game_state

    def get_legal_moves(self):
        """
        :return: A list of legal moves in the current board state
        """
        legal_moves = []
        for position in range(len(self.game_state)):
            if self.game_state[position] == " ":
                legal_moves.append(position)
        return legal_moves

    def valid_token(self, token) -> bool:
        """
        Checks to make sure an input is "X" or "O"
        """
        is_ = token == ' '
        is_x = token == 'X' or token == 'x'
        is_o = token == 'O' or token == 'o'
        is_valid = is_ or is_x or is_o

        if is_valid:
            return True
        return False

    def play_move(self, token, location):
        """
        Place an X or and O at the given location

        Location is an index in an array corresponding to a board location

         0 | 1 | 2
         3 | 4 | 5
         6 | 7 | 8

        """
        if location > 8 or location < 0:
            raise IndexError('Board only has index 0-8 (got ' + str(location) + ')')

        if token.upper() != self.whos_turn:
            raise RuntimeError('It is ' + str(self.whos_turn) + "'s turn!")

        if not self.valid_token(token):
            raise RuntimeError('Illegal move! Must be "X" or "O" (got "' + str(token) + '")')

        current_value = self.game_state[location]
        if current_value != ' ':
            raise RuntimeError("Illegal move! Location " + str(location) + " already has an " + str(current_value))

        # If it's a legal move, update the board state
        # strings are imutable so we have to copy it over and make the change while copying
        new_state = ''
        for i in range(9):
            if i == location:
                new_state += token
            else:
                new_state += self.game_state[i]
        self.game_state = new_state

        if token.upper() == 'X':
            counter = 1
        else:
            counter = -1
        self.cell_counts[location // 3][location % 3] = counter

        # Update the row/column/diagonal sums
        self.update_sums()

        # Increment the number of moves
        self.num_moves += 1

        # Switch who's turn it is to play
        if self.whos_turn == 'X':
            self.whos_turn = 'O'
        else:
            self.whos_turn = 'X'

    def update_sums(self):
        """
        Updates all the row/column/diagonal sums.

        Each cell has either a +1 or -1 in it (+1 for X, -1 for O).
        This function sums them and updates the corresponding variables
        """
        self.row0_sum = np.sum(self.cell_counts[0])
        self.row1_sum = np.sum(self.cell_counts[1])
        self.row2_sum = np.sum(self.cell_counts[2])

        self.col0_sum = np.sum([self.cell_counts[x][0] for x in range(3)])
        self.col1_sum = np.sum([self.cell_counts[x][1] for x in range(3)])
        self.col2_sum = np.sum([self.cell_counts[x][2] for x in range(3)])

        self.dia0_sum = np.sum([self.cell_counts[x][x] for x in range(3)])
        self.dia1_sum = np.sum([self.cell_counts[x][2-x] for x in range(3)])

        self.list_of_sums = [self.row0_sum, self.row1_sum, self.row2_sum,
                             self.col0_sum, self.col1_sum, self.col2_sum,
                             self.dia0_sum, self.dia1_sum]


    def game_over(self):
        """
        Evaluates the board and returns whether the game is over

        If the game is over, it also returns the winner

        If X wins, the winner is "X"
        If O wins, the winner is "O"
        If it's a draw, the winner is "draw"

        :return: (bool, winner)
        """
        for checksum in self.list_of_sums:
            if checksum == 3:
                return True, "X"
            elif checksum == -3:
                return True, "O"

        # If we got here without a winner, then it's either a draw or still going on
        if self.num_moves == 9:
            return True, "draw"
        return False, None

    def clear_board(self):
        """
        Resets the board to empty
        """
        self.game_state = ' ' * 9
        self.cell_counts = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])

        self.row0_sum = 0
        self.row1_sum = 0
        self.row2_sum = 0
        self.col0_sum = 0
        self.col1_sum = 0
        self.col2_sum = 0
        self.dia0_sum = 0
        self.dia1_sum = 0

        self.list_of_sums = [self.row0_sum, self.row1_sum, self.row2_sum,
                             self.col0_sum, self.col1_sum, self.col2_sum,
                             self.dia0_sum, self.dia1_sum]




if __name__ == "__main__":
    my_board = Board()

    my_board.play_move("X", 4)
    print(my_board)

    print()
    my_board.play_move("O", 5)
    print(my_board)

    print()
    my_board.play_move('x', 7)
    print(my_board)
    print(my_board.game_over())

    my_board.play_move('o', 8)
    my_board.play_move('x', 1)
    print(my_board)
    print(my_board.game_over())
    print(my_board.cell_counts)
    print(my_board.list_of_sums)

    my_board.clear_board()
    print(my_board)
    print(my_board.cell_counts)
    print(my_board.list_of_sums)
