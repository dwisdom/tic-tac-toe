# Tic-Tac-Toe

[![pipeline status](https://gitlab.com/dwisdom/tic-tac-toe/badges/master/pipeline.svg)](https://gitlab.com/dwisdom/tic-tac-toe/commits/master)
[![code coverage](https://gitlab.com/dwisdom/tic-tac-toe/badges/master/coverage.svg)](https://gitlab.com/dwisdom/tic-tac-toe/commits/master)


A Tic-Tac-Toe engine inspired by [Menace](https://youtu.be/R9c-_neaxeU) and in desperate need of a name.

## Current Goals
* Use this project to try out software dev things
* Only use the Python standard library (maybe some Cython too)
* Train via self-play
 

## Stretch Goals
* Maybe do a MCTS version if I ever get this one to work
